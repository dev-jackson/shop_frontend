import { Component, OnInit } from '@angular/core';
import { CarService } from 'src/app/car/services/car.service';
import { Product } from '../interfaces/product.interface';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  private _products: Product[] = [];

  get products(){
    return [...this._products];
  }

  constructor( private productService: ProductService, private carService: CarService ) {
      productService.getProducts().subscribe((data:Product[]) => {
        this._products = data;
      });
  }

  addProductInCar(product: Product, amount: string){
    const currentProduct: Product = product;
    currentProduct.amount = currentProduct.amount - parseInt(amount);
    this.carService.addProduct(currentProduct);
  }

  ngOnInit(): void {
  }



}
