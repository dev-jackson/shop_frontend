import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-product-main-page',
  templateUrl: './product-main-page.component.html',
  styleUrls: ['./product-main-page.component.css']
})
export class ProductMainPageComponent implements OnInit {

  isSessionExits: boolean = false;

  constructor(private authService: AuthService) {
    this.isSessionExits = authService.isSessionExist();
   }

  ngOnInit(): void {
  }

}
