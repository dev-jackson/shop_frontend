export interface Product {
    productId: number;
    name:      string;
    amount:    number;
    price:     number;
    createdAt: Date;
}
