import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../shared/shared.module';
import { ProductRoutingModule } from './product-routing.module';
import { ProductMainPageComponent } from './product-main-page/product-main-page.component';
import { CarModule } from '../car/car.module';



@NgModule({
  declarations: [
    ListComponent,
    ProductMainPageComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    CarModule
  ]

})
export class ProductModule { }
