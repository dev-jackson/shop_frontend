import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductMainPageComponent } from './product-main-page/product-main-page.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'main',
        component: ProductMainPageComponent
      },
      {
        path: '**',
        redirectTo: 'main'
      }
    ]
  }
]



@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule { }
