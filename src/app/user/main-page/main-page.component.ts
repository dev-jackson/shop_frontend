import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/auth/interfaces/user.interface';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  isActiveSession: boolean = false;
  user?: User ;

  constructor( private authServise: AuthService ) { 
    this.isActiveSession = authServise.isSessionExist();
    if(this.isActiveSession){
      this.user = JSON.parse(localStorage.getItem('user')!);
    }
  }

  ngOnInit(): void {
  }

}
