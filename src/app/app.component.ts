import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shopUserBackEnd';
  constructor(private route: Router, private authService: AuthService){
    if(this.authService.isSessionExist()){
      this.route.navigate(['/product']);
    }else{
      
    }
  }
}
