export interface Address {
    addressId: number;
    work:      string;
    recidence: string;
    phone:     string;
    createdAt: Date;
}
