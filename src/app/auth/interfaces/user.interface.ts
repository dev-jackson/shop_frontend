import { Address } from "./address.interface";

export interface UserLogin {
    email: string,
    password: string
}

export interface User {
    userId:    number;
    addressId: number;
    age:       number;
    fristName: string;
    lastName:  string;
    email:     string;
    password:  string;
    createdAt: Date;
    address:   Address;
}
