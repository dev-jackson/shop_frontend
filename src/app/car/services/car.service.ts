import { Injectable } from "@angular/core";
import { Product } from "src/app/product/interfaces/product.interface";



@Injectable({
    providedIn: 'root'
})
export class CarService {

    private _carProducts: Product[] = [];
    private _totalPrice: number = 0;

    get totalPrice(){
        return this._totalPrice;
    }

    get carProducts(): Product[] {
        return [...this._carProducts];
    }


    addProduct(product: Product){

        if(!this._carProducts.includes(product)){
            this._carProducts.push(product)
        }
    }
}