import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/product/interfaces/product.interface';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-item-car',
  templateUrl: './item-car.component.html',
  styleUrls: ['./item-car.component.css']
})
export class ItemCarComponent implements OnInit {


  get productsInCar(): Product[]{
    return [...this.carService.carProducts];
  }

  constructor(private carService: CarService) { 
  }

  ngOnInit(): void {
  }

}
