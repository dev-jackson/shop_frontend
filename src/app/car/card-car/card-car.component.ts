import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/product/interfaces/product.interface';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-card-car',
  templateUrl: './card-car.component.html',
  styleUrls: ['./card-car.component.css']
})
export class CardCarComponent implements OnInit {

  total: number = 0;

  constructor(private carService: CarService) {
    this.total = carService.totalPrice;
  } 

  ngOnInit(): void {
  }

}
