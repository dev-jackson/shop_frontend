import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemCarComponent } from './item-car/item-car.component';
import { CardCarComponent } from './card-car/card-car.component';



@NgModule({
  declarations: [
    ItemCarComponent,
    CardCarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardCarComponent
  ]
})
export class CarModule { }
